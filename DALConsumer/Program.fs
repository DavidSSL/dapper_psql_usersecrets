﻿// Learn more about F# at http://fsharp.org

open System
open DAL.Queries
[<EntryPoint>]
let main argv =
(*
  getDbs
  |> Seq.iter(fun n -> printfn "%s" n.datname)
*)
  getActors "SELECT first_name, last_name, last_update FROM actor"
  |> Seq.iter(fun a -> printfn "%s %s %A" a.first_name a.last_name a.last_update)
  0 // return an integer exit code
