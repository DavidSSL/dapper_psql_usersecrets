﻿namespace DAL

open System
open System.IO
open Npgsql
open Dapper
open Microsoft.Extensions.Configuration

module Queries =
    let hello name =
        printfn "Hello %s" name

    type Database =
        { datname: string }

    type Actor =
        { first_name: string
          last_name: string
          last_update: DateTime }

    let getDbs =
        let builder = ConfigurationBuilder()
        let config =
            builder
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", false, true)
                .AddUserSecrets<Database>()
                .Build()

        let dbHostname = config.GetSection("Database:Localhost:Hostname").Value
        let dbUsername = config.GetSection("Database:Localhost:Username").Value
        let dbPassword = config.GetSection("Database:Localhost:Password").Value

        let connStr = sprintf "Host=%s;Database=postgres;Username=%s;Password=%s;" dbHostname dbUsername dbPassword
        let conn = new NpgsqlConnection(connStr)
        use dbConn = conn
        dbConn.Open()
        dbConn.Query<Database>("SELECT datname FROM pg_database WHERE datistemplate=FALSE;") |> Seq.cast<Database>
        
    let getActors query =
        let builder = ConfigurationBuilder()
        let config =
            builder
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", false, true)
                .AddUserSecrets<Database>() // I don't know why passing Database works here but I can't use the non generic version!!
                .Build()

        let dbHostname = config.GetSection("Database:Localhost:Hostname").Value
        let dbUsername = config.GetSection("Database:Localhost:Username").Value
        let dbPassword = config.GetSection("Database:Localhost:Password").Value

        let connStr = sprintf "Host=%s;Database=postgres;Username=%s;Password=%s;" dbHostname dbUsername dbPassword
        let conn = new NpgsqlConnection(connStr)
        use dbConn = conn
        dbConn.Open()
        dbConn.Query<Actor>(sprintf "%s" query) |> Seq.cast<Actor>
