# Introduction

This is a tutorial about how to do a basic set up of:

1. [Dapper](https://github.com/StackExchange/Dapper) with [PostgreSQL](https://www.postgresql.org)
2. [User secrets](https://docs.microsoft.com/en-us/aspnet/core/security/app-secrets?view=aspnetcore-3.1&tabs=linux)

all with F#.

We will be using nuget but it's also possible to use [paket](https://fsprojects.github.io/Paket/) in terms of package management.

This is not a tutorial about any of the various components e.g. Dapper but instead is meant to be a guide on how to integrate the various pieces of technology.

# Requirements

This is the environment on which the following tutorial was tested:

- dotnet core version 3.1.100
  ```bash
  dotnet --version
  ```

- docker 19.03.5-ce, build 633a0ea838
  ```bash
  docker --version
  ```

We are using docker in order to host our PostgreSQL database containing data.

# Dapper and PostgreSQL

What we want to achieve is set up Dapper to talk to our PostgreSQL database.

We'll start off by setting up the database and the most straightforward way to do that is to use docker.


## Run docker container for our PostgreSQL database

```bash
docker run -e POSTGRES_PASSWORD=yourpassword -p 5432:5432 -d frantiseks/postgres-sakila
```

This is a [PostgreSQL database](https://hub.docker.com/r/frantiseks/postgres-sakila/) port of the  [MySQL Sakila data](https://dev.mysql.com/doc/sakila/en/).

After the container has started, you should be able to run an SQL query.

![](doc_images/table_content.png)

In this case this is showing the content of the actor table.


## Create the dotnet core projects

We will be creating 2 projects:

1. The database access layer library project , `DAL`
2. The console application project, `DALConsumer`, which will call the 1st project.


### Create the database access layer library project

```bash
dotnet new library -lang F# -n "DAL"
cd DAL # Go into the newly created project directory
```

This is the project which will directly interact with our PostgreSQL database and hence we need to add the following packages:

```bash
dotnet add package Dapper --version 2.0.30
dotnet add package Npgsql --version 4.1.2
cd .. # Ensure you are no longer in the DAL folder
```


### Create the DAL consumer console application

```bash
dotnet new console -lang F# -n "DALConsumer"
```
### Create solution file and add projects to solution

```bash
dotnet new sln -n "dapper_psql_usersecrets"
dotnet sln add DAL/DAL.fsproj
dotnet sln add DALConsumer/DALConsumer.fsproj
```


### Query the data using Dapper

What we will do is retrieve the databases from this particular server and it shouldn't be a surprise that we will get only 1 result in this case, which is `postgres`.

This is done to deliberately keep things simple.

The SQL to do this is:

```SQL
SELECT datname
FROM pg_database
WHERE datistemplate = FALSE
```

and we'll use it later.

If we go to our `DAL` library project, there is a file `Library.fs`

![](doc_images/original_library.png)

We will change the module name to be slightly better:

```FSharp
module Queries =
```

Let's modify `DAL.Queries` in order to make the call to the database.

First we'll have to reference both Npgsql and Dapper:

```bash
open Npgsql
open Dapper
```

Then we'll have to create a new type which will be our Model for our database result query:

```FSharp
type Database = {datname:string}
```

The key name matters as does the casing and it should match the name returned from the dB query.

Using `Datname` would cause an exception.

Even aliasing the column name won't work which means that we'll have to simply use the column name as it is in the dB.

Now add a new function in the Queries module:

```FSharp
    let getDbs =
        let connStr ="Host=YourHost;Database=postgres;Username=YourUserName;Password=yourpassword;" // 1
        let conn = new NpgsqlConnection(connStr)
        use dbConn = conn
        dbConn.Open()
        dbConn.Query<Database>("SELECT datname FROM postgres WHERE datistemplate=FALSE;") |> Seq.cast<Database> // 2
```

There are 2 points worth making:

1. Our connection string contains sensitive information and ideally this information should be kept secret. For development purposes, we'll use user secrets which, while it doesn't encrypt our data, helps mitigate exposing sensitive information.
2. We are performing a cast because we want to return `seq<Database>` rather than an `IEnumerable<Database>`.

This function `getDbs` will return a sequence of `Database` records and if we now go to our `DALConsumer`, we can display the data being returned.

```FSharp
let main argv =
  getDbs
  |> Seq.iter(fun n -> printfn "%s" n.datname)
```

However, I would advise that you first put the connection string sensitive information in user secrets first and this process is described in the next section

### Put sensitive information in user secrets

[User secrets](https://docs.microsoft.com/en-us/aspnet/core/security/app-secrets?view=aspnetcore-3.1&tabs=linux) is a tool offered by Microsoft used for storing sensitive information outside of our repository as the latter is usually under version control and ideally we should not be storing such information under version control.

The user secrets tool will *not* encrypt the data though.

The mechanics of how to use the tool is described [here](https://docs.microsoft.com/en-us/aspnet/core/security/app-secrets?view=aspnetcore-3.1&tabs=linux) and this tutorial will not cover this ground in detail.

Let's assume the following:

| Variable | Value |
| -------- | ----- |
| Hostname | localhost |
| Database | postgres |
| Username | postgres |
| Password | password |

what we want to do is to store Hostname, Username and Password as user secrets.

So we have to initialise user secrets for our solution

```bash
dotnet user-secrets init --project pathToProject
```

As a result, our project file includes our UserSecretsId:

![](doc_images/user_secrets_id.png)

#### Set user secrets

We will now set the secrets:

```bash
dotnet user-secrets set "Database:Localhost:Hostname" "localhost" --project DAL
dotnet user-secrets set "Database:Localhost:Username" "postgres" --project DAL
dotnet user-secrets set "Database:Localhost:Password" "password" --project DAL
```

If you go to `~/.microsoft/usersecrets/004fc1a0-8ce9-4105-821f-d2ee9c14893e/secrets.json` you will see that the secrets have been set:

![](doc_images/user_secrets_json.png)

Usually we also have configuration whose values we don't necessarily want to put in our user secrets.

The file we normally use in such cases is the `appsettings.json`.

```json
{
  "Logging": {
    "LogLevel": {
      "Default": "Debug",
      "System": "Information",
      "Microsoft": "Information"
    }
  },
  "Database": {
    "Localhost": {
      "Hostname": "For development this is stored in user secrets",
      "Username": "For development this is stored in user secrets",
      "Password": "For development this is stored in user secrets"
    }
  }
}
```

Hence we can have our secrets stored someplace else while also having non-sensitive information mixed in.

However, this is not enough because we need to ensure that this file is copied to our publish directory and therefore we need to amend our project file:

```xml
  <ItemGroup>
    <Content Include="appsettings.json">
      <CopyToOutputDirectory>PreserveNewest</CopyToOutputDirectory>
    </Content>
  </ItemGroup>
```

#### Retrieve user secrets in code

You first have to add the `Microsoft.Extensions.Configuration` nuget package:

```bash
dotnet add DAL/DAL.fsproj package Microsoft.Extensions.Configuration.UserSecrets --version 3.1.1
```

Then you have to reference it in the `DAL` project

```FSharp
open Microsoft.Extensions.Configuration
```

Here is the code required in order to get the user secrets out:

```FSharp
        let builder = new ConfigurationBuilder()
        let config = builder
                        .SetBasePath(Directory.GetCurrentDirectory())
                        .AddJsonFile("appsettings.json", false, true)
                        .AddUserSecrets<Database>()
                        .Build()

        let dbHostname = config.GetSection("Database:Localhost:Hostname").Value
        let dbUsername = config.GetSection("Database:Localhost:Username").Value
        let dbPassword = config.GetSection("Database:Localhost:Password").Value
```

Then you will have to patch these values into the connection string:

```FSharp
        let connStr = sprintf "Host=%s;Database=postgres;Username=%s;Password=%s;" dbHostname dbUsername dbPassword
```

Once that's done we can test it out by going to `DALConsumer`:

```FSharp
open System
open DAL.Queries
[<EntryPoint>]
let main argv =
  getDbs
  |> Seq.iter(fun n -> printfn "%s" n.datname)
  0 // return an integer exit code
```

Then we have to build and run the application:


```bash
dotnet build
cd DALConsumer/bin/Debug/netcoreapp3.1
dotnet DALConsumer.dll
```

and you should get `postgres` as output.